REM feed in the flags for the low-end version
@echo \toggletrue{lowend} > config/flags.tex
REM double-compile (because of tikz) the low-end version and rename it
@xelatex -interaction=nonstopmode -quiet main.tex
@xelatex -interaction=nonstopmode -quiet main.tex
@move main.pdf main-reduced.pdf

REM kill low-end flag and double-compile for high-end
@echo \togglefalse{lowend} > config/flags.tex
@xelatex -interaction=nonstopmode -quiet main.tex
@xelatex -interaction=nonstopmode -quiet main.tex

REM clean up miscellaneous files
REM I don't know why texput.log sometimes appears from the ether, but it does.
@del main.aux & del main.log & del main.toc & del texput.log